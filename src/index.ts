#!/usr/bin/env node


import { reactJsInit, reactNativeInit } from "./inc/react";



const program = require('commander');

program
  .version('0.0.1')
  .description("An CLI to init your project with your desire dependencies in one line")
  .option('-r, --react-js [name]', 'ReactJS')
  .option('-R, --react-native [name]', 'React Native')

program.parse(process.argv)

if (program.reactJs !== undefined && program.reactJs !== true) {
  reactJsInit(program.reactJs);
}
if (program.reactNative !== undefined && program.reactNative !== true) {
  reactNativeInit(program.reactNative);
}

if (!process.argv.slice(2).length || process.argv[3] === undefined) {
  console.log('');
  program.outputHelp();
}
