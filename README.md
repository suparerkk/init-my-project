# Init my project

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

### Tech

* [Node.js]


### Installation

requires [Node.js](https://nodejs.org/) v4+ to run.

Install the dependencies and devDependencies
Open your favorite Terminal and run these commands.

```sh
$ npm install -d
$ npm run create
```

### Customization
##### Add more dependencies to your project
src/inc/dep.ts
```javascript
//-----  Installing dependencies -----
  export const rjdeps = ['react-router-dom'];
  export const rndeps = ['']
````

License
----

MIT


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   
   [Node.js]: <http://nodejs.org>