#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = require("./inc/react");
var program = require('commander');
program
    .version('0.0.1')
    .description("An CLI to init your project with your desire dependencies in one line")
    .option('-r, --react-js [name]', 'ReactJS')
    .option('-R, --react-native [name]', 'React Native');
program.parse(process.argv);
if (program.reactJs !== undefined && program.reactJs !== true) {
    react_1.reactJsInit(program.reactJs);
}
if (program.reactNative !== undefined && program.reactNative !== true) {
    react_1.reactNativeInit(program.reactNative);
}
if (!process.argv.slice(2).length || process.argv[3] === undefined) {
    console.log('');
    program.outputHelp();
}
