"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var chalk = require('chalk');
var shell = require('shelljs');
var fs = require("fs");
var dep_1 = require("./dep");
//-----##### REACT JS #####-----
//-----##### REACT JS #####-----
//-----##### REACT JS #####-----
function reactJsInit(projectName) {
    //----- Check for exist -----
    if (fs.existsSync('./' + projectName)) {
        console.log(chalk.white.bgRed.bold('\nFolder is exist'));
        console.log('Please change project name or delete folder\n');
        process.exit();
    }
    //----- Create-react-app -----
    console.log('Project: ReactJS');
    console.log('Name: ' + projectName);
    console.log(chalk.white.bgGreen.bold('Creating project environment..'));
    console.log(chalk.black.bgWhite.bold('> npx create-react-app ' + projectName));
    shell.exec('npx create-react-app ' + projectName);
    //-----  Installing dependencies -----
    var deps = dep_1.rjdeps;
    var commandsDep = ['cd ' + projectName, ''];
    var zeroInit = 1;
    deps.forEach(function (dep) {
        commandsDep[zeroInit] = 'npm install ' + dep;
        zeroInit++;
    });
    commandsDep.forEach(function (cmdDep) {
        console.log(chalk.black.bgWhite.bold('> ' + cmdDep));
        shell.exec(cmdDep);
    });
    //----- Done -----
    console.log(chalk.white.bgGreen.bold('Your ReactJS "' + projectName + '" is created!\n'));
    console.log('We suggest that you begin by typing:\n');
    console.log('cd ' + projectName);
    console.log('yarn start \n');
}
exports.reactJsInit = reactJsInit;
//-----##### REACT NATIVE #####-----
//-----##### REACT NATIVE #####-----
//-----##### REACT NATIVE #####-----
function reactNativeInit(projectName) {
    //----- Check for exist -----
    if (fs.existsSync('./' + projectName)) {
        console.log(chalk.white.bgRed.bold('\nFolder is exist'));
        console.log('Please change project name or delete folder\n');
        process.exit();
    }
    //----- Preparing -----
    console.log(chalk.white.bgGreen.bold('Prepare for creating project environment'));
    console.log(chalk.black.bgWhite.bold('> brew tap AdoptOpenJDK/openjdk'));
    shell.exec('brew tap AdoptOpenJDK/openjdk');
    console.log(chalk.black.bgWhite.bold('> brew cask install adoptopenjdk8'));
    shell.exec('brew cask install adoptopenjdk8');
    console.log('\n');
    console.log(chalk.black.bgWhite.bold('> npm install -g react-native-cli'));
    shell.exec('npm install -g react-native-cli');
    //----- Create-react-app -----
    console.log('Project: ReactJS');
    console.log('Name: ' + projectName);
    console.log(chalk.white.bgGreen.bold('Creating project environment..'));
    console.log(chalk.black.bgWhite.bold('> react-native init ' + projectName));
    shell.exec('react-native init ' + projectName);
    //-----  Installing dependencies -----
    var deps = dep_1.rndeps;
    var commandsDep = ['cd ' + projectName, ''];
    var zeroInit = 1;
    deps.forEach(function (dep) {
        commandsDep[zeroInit] = 'npm install ' + dep;
        zeroInit++;
    });
    commandsDep.forEach(function (cmdDep) {
        console.log(chalk.black.bgWhite.bold('> ' + cmdDep));
        shell.exec(cmdDep);
    });
    //----- Done -----
    console.log(chalk.white.bgGreen.bold('Your React Native "' + projectName + '" is created!\n'));
    console.log('We suggest that you begin by typing:\n');
    console.log('cd ' + projectName);
    console.log('react-native run-ios \n');
}
exports.reactNativeInit = reactNativeInit;
